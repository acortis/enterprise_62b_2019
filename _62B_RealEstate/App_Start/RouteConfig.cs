﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace _62B_RealEstate
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Allow routing through attributes
            // Necessary, otherwise, attributes routes are ignored...
            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "StandardSearch",
                url: "{buildingType}-in-{locality}",
                defaults: new { controller = "Search", action = "Search" }
                );
            /*
            // listingsearch /{ locality}/{ action}
            // ~/ listingsearch / sliema / search 
            // Action name is optional
            routes.MapRoute(
                name: "Listing Search",
                url: "ListingSearch/{locality}/{action}",
                defaults: new { controller="Search", action = "Search" }
                );

            routes.MapRoute(
                name: "Browsing",
                url: "browse/{controller}-{action}/{locality}",
                defaults: new { locality="Valletta" }
                );

            // luxury /{ propertytype}/{ locality}
            // ~/ luxury / palace / valletta Default to Mdina townhouses
            routes.MapRoute(
                name: "Luxury",
                url: "luxury/{propertytype}/{locality}",
                defaults: new { controller = "Search", action = "Search", locality="Mdina", propertytype="townhouses" }
                );
                */

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            
        }
    }
}

﻿using _62B_RealEstate.Filters;
using System.Web;
using System.Web.Mvc;

namespace _62B_RealEstate
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new LogAttribute());    // Every action will make use of our custom logging...
        }
    }
}

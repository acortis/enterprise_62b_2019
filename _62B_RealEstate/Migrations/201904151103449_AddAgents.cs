namespace _62B_RealEstate.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAgents : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Agents",
                c => new
                    {
                        ID = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false),
                        IsAdmin = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);

            // Sql("INSERT INTO Agents VALUES ('123456M', 'Andrew', 1)");
        }
        
        public override void Down()
        {
            DropTable("dbo.Agents");
        }
    }
}

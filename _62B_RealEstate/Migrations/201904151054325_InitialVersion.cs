namespace _62B_RealEstate.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialVersion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Properties",
                c => new
                    {
                        Reference = c.String(nullable: false, maxLength: 128),
                        Locality = c.String(),
                        PropertyType = c.String(),
                        Bedrooms = c.Int(nullable: false),
                        ContractType = c.String(),
                        Description = c.String(),
                        FloorArea = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ImageURL = c.String(),
                    })
                .PrimaryKey(t => t.Reference);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Properties");
        }
    }
}

namespace _62B_RealEstate.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<_62B_RealEstate.Models.RealEstateContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(_62B_RealEstate.Models.RealEstateContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            context.Agents.AddOrUpdate(
                    new Models.Agent() { ID = "1234M", Name = "Andrew", IsAdmin = true }
                );
        }
    }
}

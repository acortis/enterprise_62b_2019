﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(_62B_RealEstate.Startup))]
namespace _62B_RealEstate
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

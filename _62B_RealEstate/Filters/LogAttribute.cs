﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _62B_RealEstate.Filters
{
    public class LogAttribute : ActionFilterAttribute
    {
        /*
                IList<ExceptionDetail> exceptionList;

                HttpContext.Current.Application.Lock();     // Application Context is locked, and only 1 thread can access this code

                try
                {
                    if (HttpContext.Current.Application["ExceptionList"] != null)
                    {
                        exceptionList = (IList<ExceptionDetail>) HttpContext.Current.Application["ExceptionList"];
                    }
                    else
                    {
                        exceptionList = new List<ExceptionDetail>();

                        HttpContext.Current.Application.Add("ExceptionList", exceptionList);
                    }

                    var exceptionDetail = new ExceptionDetail()
                    {
                        Exception = filterContext.Exception,
                        Time = DateTime.Now,
                        User = filterContext.HttpContext.User
                    };

                    exceptionList.Add(exceptionDetail);
                }
                finally // make sure that the ApplicationState is unlocked!
                {
                    HttpContext.Current.Application.UnLock();
                }
        } */



        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext.Current.Application.Lock();  // Application Context is locked, and only 1 thread can access this code

            string username = filterContext.HttpContext.User.Identity.Name;
            string url = filterContext.HttpContext.Request.RawUrl;
            try
            {
                IList<String> logInformation = null;

                // this section of the code loads the log from the Application memory (if it exists)
                if (HttpContext.Current.Application["Log"] == null)
                {
                    logInformation = new List<String>(); // start logging from scratch!
                    HttpContext.Current.Application["Log"] = logInformation; // remember what we are logging for later...
                }
                else
                {
                    logInformation = (IList<String>)HttpContext.Current.Application["Log"]; // load whatever we had saved here before...
                }

                // start logging here!!
                logInformation.Add(String.Format("The user {0} has accessed {3} on {1} at {2}", username, DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), url));
            }
            finally
            {
                HttpContext.Current.Application.UnLock(); // Make sure to unlock the door behind you!!! - even if there is an exception!
            }
           
            base.OnActionExecuting(filterContext);
        }



        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _62B_RealEstate.Models
{
    public class RealEstateContext : DbContext
    {
        public RealEstateContext() : base("RealEstateConnection")
        {
        }

        public DbSet<Property> Properties { get; set; }
        public DbSet<Agent> Agents { get; set; }

    }
}
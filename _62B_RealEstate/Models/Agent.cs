﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _62B_RealEstate.Models
{
    public class Agent
    {
        [Key]
        [Required]
        public String ID { get; set; }

        [Required]
        public String Name { get; set; }
        
        [Required]
        public bool IsAdmin { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _62B_RealEstate.Helpers
{
    public static class ExtensionMethods
    {
        public static void Swap<T>(this List<T> list, int i, int j)
        {
            T temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }
    }
}
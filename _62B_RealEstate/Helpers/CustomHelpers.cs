﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _62B_RealEstate.Helpers
{
    public static class CustomHelpers
    {
        public static MvcHtmlString GenerateTitle(String title)
        {
            return MvcHtmlString.Create("<h4>" + title + "</h4>");
        }
    }
}
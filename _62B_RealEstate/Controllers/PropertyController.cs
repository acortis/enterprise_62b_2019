﻿using _62B_RealEstate.Filters;
using _62B_RealEstate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _62B_RealEstate.Helpers;

namespace _62B_RealEstate.Controllers
{
    public class PropertyController : Controller
    {
        RealEstateContext realEstateContext = new RealEstateContext();

        // GET: Property/Details - the default id is ABC123
        // GET: Property/Details/5
        public ActionResult Details(String id = "ABC123")
        {
            //Property myProperty = (Property)realEstateContext.Properties.Where(x => x.Reference == id).SingleOrDefault();

            Property myProperty = new Property()
            {
                Locality = "Sliema",
                PropertyType = "Apartment",
                Reference = id,
                Bedrooms = 3,
                ContractType = "Sale",
                FloorArea = 200,
                Price = 300000,
                Description = "Second floor Apartment in a new block. Property consists of an open plan kitchen/living/dining room, two double bedrooms, en suite, a main bathroom and two balconies. Apartment is luxury finished without bathrooms and internal door. Optional garage.",
                ImageURL = @"\Images\listing.jpg"
            };

            // The view will accept Property as a Model
            return View(myProperty); // no view available so far...
        }

        public ActionResult Index(String id = "Sliema")
        {
            List<Property> properties = new List<Property>();

            properties.Add(
                new Property()
                {
                    Locality = id,
                    PropertyType = "Apartment",
                    Reference = "ABC123",
                    Bedrooms = 3,
                    ContractType = "Sale",
                    FloorArea = 100,
                    Price = 200000,
                    Description = "Second floor Apartment in a new block. Property consists of an open plan kitchen/living/dining room, two double bedrooms, en suite, a main bathroom and two balconies. Apartment is luxury finished without bathrooms and internal door. Optional garage.",
                    ImageURL = @"\Images\listing.jpg"
                }
              );

            properties.Add(
                new Property()
                {
                    Locality = id,
                    PropertyType = "Apartment",
                    Reference = "ABC456",
                    Bedrooms = 3,
                    ContractType = "Sale",
                    FloorArea = 100,
                    Price = 200000,
                    Description = "Second floor Apartment in a new block. Property consists of an open plan kitchen/living/dining room, two double bedrooms, en suite, a main bathroom and two balconies. Apartment is luxury finished without bathrooms and internal door. Optional garage.",
                    ImageURL = @"\Images\listing.jpg"
                }
              );

            properties.Add(
                new Property()
                {
                    Locality = id,
                    PropertyType = "Apartment",
                    Reference = "ABC789",
                    Bedrooms = 3,
                    ContractType = "Sale",
                    FloorArea = 100,
                    Price = 200000,
                    Description = "Second floor Apartment in a new block. Property consists of an open plan kitchen/living/dining room, two double bedrooms, en suite, a main bathroom and two balconies. Apartment is luxury finished without bathrooms and internal door. Optional garage.",
                    ImageURL = @"\Images\listing.jpg"
                }
              );

            properties.Swap(0, 1);

            IEnumerable<Property> model = (IEnumerable<Property>) properties;

            ViewBag.Location = id;
            ViewBag.PropertiesFound = 3;

            return View(model);
        }
    }
}
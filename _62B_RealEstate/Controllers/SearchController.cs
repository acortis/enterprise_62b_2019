﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _62B_RealEstate.Controllers
{
    public class SearchController : Controller
    {
        public ActionResult Index()
        {
            return Content("Index");
        }

        public ActionResult Search()
        {
            return Content("Search");
        }

        public ActionResult Search(String locality)
        {
            return Content("Search for locality " + locality);
        }

        public ActionResult MostViewed(String locality)
        {
            return Content("Most viewed for locality " + locality);
        }
    }
}
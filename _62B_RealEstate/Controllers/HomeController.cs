﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _62B_RealEstate.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(String id)
        {
            if (id != null)
                return Content("Home Index with id: " + id);

            return View();
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public FilePathResult GetImage()
        {
            return File(Server.MapPath("~/App_Data/property.jpg"), "image/jpg");
        }

        public RedirectResult RedirectLink()
        {
            return Redirect("About");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _62B_RealEstate.Controllers
{
    [RoutePrefix("Listing")]
    public class ListingSearchController : Controller
    {
        // GET: ListingSearch
        [Route("50PercentSale")]
        public ContentResult Index()
        {
            return Content("Listing Search Index");
            //return View();
        }

        [Route("Other")]
        public ActionResult OtherMethod()
        {
            return Content("");
        }

    }
}